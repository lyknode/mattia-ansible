#!/bin/bash
#
# {{ ansible_managed }}
#


set -ueo pipefail

first_year=2013
curr_year="$(date +%Y)"

log_dir=/srv/irc.mapreri.org/htdocs/logs/


echo "Compressing up to $(("$curr_year"-1)) IRC logs..."

cd "$log_dir"
for y in $(seq "$first_year" "$(("$curr_year"-1))"); do
    echo "Operating on year $y..."
    find . -path "./*/$y/*" -name '*.log' -exec gzip {} \;
done

echo "Compression done!"
echo "Update HTML..."
logs2html .
echo "All done!"
