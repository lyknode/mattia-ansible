#!/usr/bin/python3
# vim: set fileencoding=utf-8 :
#
# {{ ansible_managed }}
#
# Copyright © 2014-2015 Mattia Rizzolo <mattia@mapreri.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# On Debian systems, the complete text of the GNU General
# Public License can be found in `/usr/share/common-licenses/GPL-3'.


"""
TODO:
* the handling of log filenames should definitely be more flexible
* paths should be far more uniform
* implement something to translate xchat logs format in energymech format
  (to be hable to submit it to pisg (and dunno if irc2html is able to
  handle it anyway))
"""

import os
import re
import sys
import html
import time
import yaml
import filecmp
import logging
import argparse
import subprocess
from string import Template
from shutil import copy, move
from datetime import datetime

# XXX get rid of this
settings_file = '/srv/irc.mapreri.org/bin/settings'

pisg_template = Template(
    '<channel="$channel">'
    '    Logfile = "$logdir/$network/$channel/*/*.log.gz'
    '    Logfile = "$logdir/$network/$channel/*/*.log"'
    '    Network = "$network"'
    '    OutputFile = "$statsdir/$network/$channelname.html"'
    '</channel>'
)

if not (sys.version_info[0] >= 3 and sys.version_info[1] >= 5):
    print('error, I really need at least python 3.5 here', file=sys.stderr)
    sys.exit(1)


def call(to_call):
    isinstance(to_call, list)
    try:
        logger.debug('\t\tCalling `%s`', ' '.join(to_call))
        subprocess.run(to_call, check=True)
    except subprocess.CalledProcessError as e:
        logger.critical(
            '%s failed with error code %s (cmd line: `%s`)' %
            (to_call[0], e.returncode, e.cmd)
        )
        crash(e)


def crash(exception):
    logger.critical(
        'The program is quitting due to an unrecovereble error '
        'or nonsense.  Terminating now and leaving the lock file in place '
        'place, so no further harm can be done.')
    logger.exception('Logged traceback:')
    logger.critical('-------------------------------------------------------')
    raise exception


class Configuration:
    def __init__(self, settings):
        self.settings_file = settings
        self.load()

    def load(self):
        try:
            with open(self.settings_file, "r") as fd:
                s = yaml.safe_load(fd)
        except FileNotFoundError:
            print(f"Settings file {self.settings_file} not found", file=sys.stderr)
            sys.exit(1)
        j = os.path.join
        self.basepath = s['basepath']
        self.originlog = s['originlog']
        self.logfile = j(*s['logfile'])
        self.finaldir = j(*s['finaldir'])
        self.statsdir = j(*s['statsdir'])
        self.privatelogs = j(*s['privatelogs'])
        self.errorlogs = j(*s['errorlogs'])
        self.pisgcfg = j(*s['pisgcfg'])
        self.pisgd = j(*s['pisgd'])
        self.site = s['site']
        self.encodings = s['encodings']
        self.blacklisted_channels = s['blacklisted_channels']
        self.blacklisted_servers = s['blacklisted_servers']
        self.blacklisted_users = s['blacklisted_users']


conf = Configuration(settings_file)


class Lock:
    lockfile = '/run/lock/irc-loghandler.lock'

    @staticmethod
    def lock():
        if not args.force:
            logger.debug('Locking the state...')
            with open(Lock.lockfile, 'w') as f:
                msg = 'locked by the loghandler process at %s' % datetime.now()
                f.write(msg)
        else:
            logger.debug("args.force in place, not locking!")

    @staticmethod
    def unlock():
        if not args.force:
            logger.debug('Unlocking the state...')
            os.remove(Lock.lockfile)
        else:
            logger.debug("args.force in place, not trying to remove lockfile")

    @staticmethod
    def check():
        ''' Return true if the lockfile is present'''
        return False if args.force else os.access(Lock.lockfile, os.F_OK)

    @staticmethod
    def check_or_quit(timeout=0):
        '''
        timeout: exit without error if the lockfile is newer than ``timeout``.
        '''
        if not Lock.check():
            return True
        if os.path.getmtime(Lock.lockfile) < time.time() - timeout:
            logger.critical('There is a lock at %s. Aborting the process now!',
                            Lock.lockfile)
            sys.exit(10)
        else:
            logger.info("There is a lock at %s, but it's too new.  Aborting!",
                        Lock.lockfile)
            sys.exit(10)


class Pisg:
    cmdline = ("pisg", "--silent", "-co", conf.pisgcfg)

    @staticmethod
    def run():
        os.makedirs("/var/cache/pisg/pisg_cache", exist_ok=True)
        logger.info("Calling pisg...")
        try:
            subprocess.run(Pisg.cmdline, check=True)
        except subprocess.CalledProcessError as e:
            if e.returncode == -9:
                logger.warning("pisg SIGKILLed (probably OOM )")
                return
            crash(e)

    @staticmethod
    def create_config(log):
        if log.network == 'default':
            # I don't want to cumpute the stats for the "default" network
            logger.info('\tNot computing stats for the "default" network')
            return
        pisg_conf_file = '%s/%s_%s.cfg' % (conf.pisgd, log.network, log.channel)
        pisg_stats_dir = os.path.join(conf.statsdir, log.network)
        logger.debug("\tpisg_conf_file: %s", pisg_conf_file)
        logger.debug("\tpisg_stats_dir: %s", pisg_stats_dir)

        # be sure the channel conf file exists
        if not os.access(pisg_conf_file, os.W_OK):
            pisg_chan_conf = pisg_template.substitute(
                logdir=conf.finaldir,
                statsdir=conf.statsdir,
                network=log.network,
                channel=log.channel,
                channelname=log.channel.replace('#', '')
            )
            logger.debug("\tpisg_conf_file: %s", pisg_conf_file)
            with open(pisg_conf_file, 'w') as f:
                f.writelines(pisg_chan_conf)
            # be sure there is an include line to the pisg.cfg file
            with open(conf.pisgcfg, 'r') as pisg_conf:
                regexp = r'%s_%s\.cfg' % (log.network,
                                          log.channel.replace(r'#', r'\#'))
                config_found = False
                for line in pisg_conf:
                    if re.search(regexp, line):
                        config_found = True
                        break
            if not config_found:
                with open(conf.pisgcfg, 'a') as pisg_conf_a:
                    confline = '<include="%s">' % pisg_conf_file
                    logger.info("\tadding %s to %s", confline, conf.pisgcfg)
                    pisg_conf_a.write(confline+'\n')
            else:
                logger.debug("\tpisg conf already there, doing nothing.")
        # be sure the final directory for the stats exists
        if not os.access(pisg_stats_dir, os.W_OK):
            logger.info('\tcreating directory for pisg stats: %s',
                        pisg_stats_dir)
            os.makedirs(pisg_stats_dir, mode=0o775, exist_ok=True)


class LogLine():
    def __init__(self, line, log):
        self.line = line
        self.log = log

    def decode(self, error=False):
        for encoding in conf.encodings:
            if encoding != "utf8":
                logger.debug('\tnon-utf8 string detected on %s@%s; trying '
                             '%s on "%s"', self.log.channel, self.log.network,
                             encoding, self.line)
            try:
                return self.line.decode(encoding)
            except UnicodeDecodeError:
                pass
        logger.error("\tNon decodable string. Giving up.")
        if error:
            raise UnicodeDecodeError
        else:
            self.decodeError()
            return ''

    def decodeError(self):
        now = datetime.now().strftime("%Y-%m-%d-%H-%M-%S-%f")
        errordir = os.path.join(conf.errorlogs, 'unicodedecodeerror', now)
        dst = os.path.join(errordir, self.log.original)
        url = '%s/errorlogs/unicodedecodeerror/%s/%s' % (
            conf.site, now, html.escape(self.log.original))
        logger.error('\tUnicodeDecodeError!')
        os.makedirs(errordir, mode=0o775, exist_ok=True)
        with open(dst, 'wb') as erroroutput:
            erroroutput.writeline(self.line)
        logger.error("\tManual handling required: %s", dst)
        logger.error("\tCheck it out at %s", url)


class Log:
    def __init__(self, path):
        self.done = False
        self.original = path
        self.src = '%s/%s' % (conf.originlog, self.original)
        self._load()
        self.guess_destination()
        # used to override duplicates check after we are sure this is not dup
        self.dup_override = False

    def __str__(self):
        return '{~> Log: %s: %s/%s @ %s by %s}' % (
            self.original, self.network, self.channel,
            self.date.strftime('%Y-%m-%d'), self.user)

    def _load(self):
        # the filename is something like
        # mapreri_oftc_#debian-devel_20141126.log
        # be aware! it could also be (yeah! bad people...)
        # mapreri_oftc__rene__20141203.log
        log = self.original.rsplit('_', 1)
        timestamp = log[-1].split('.')[0]
        log.pop()  # now log is something like ['mapreri_oftc_#debian-devel']
        log = log[0].split('_', 2)  # ['mapreri', 'oftc', '_rene_']
        self.user, self.network, self.channel = log
        try:
            self.date = datetime.strptime(timestamp, "%Y%m%d")
        except ValueError:
            self.date = datetime.strptime(timestamp, "%Y-%m-%d")
        logger.debug("log: %s", self)

    def is_query(self):
        return not self.channel.startswith('#')

    def _is_blacklisted_user(self):
        return self.user in conf.blacklisted_users

    def _is_blacklisted_server(self):
        return self.network in conf.blacklisted_servers

    def _is_blacklisted_channel(self):
        return self.channel in conf.blacklisted_channels

    def is_private(self):
        return self.is_query() or \
            self._is_blacklisted_user() or \
            self._is_blacklisted_server() or \
            self._is_blacklisted_channel()

    def guess_destination(self):
        if self.is_private():
            now = datetime.now().strftime("%Y-%m-%d-%H-%M-%S")
            self._dstdirname = os.path.join(conf.privatelogs, now)
            self._dstfilename = self.original
        else:
            self._dstdirname = os.path.join(
                conf.finaldir, self.network,
                self.channel, self.date.strftime('%Y'))
            self._dstfilename = '%s_%s.log' % (
                self.channel, self.date.strftime('%Y-%m-%d'))
        self.dst = os.path.join(self._dstdirname, self._dstfilename)

    def errorlog(self, reason):
        now = datetime.now().strftime("%Y-%m-%d-%H-%M-%S-%f")
        errordir = os.path.join(conf.errorlogs, reason, now)
        os.makedirs(errordir)
        self.dst = os.path.join(errordir, self.original)
        url = '%s/errorlogs/%s/%s/%s' % (
            conf.site, reason, now, html.escape(self.original))
        logger.warning('%s! Moving the item to a staging location.', reason)
        logger.warning('Manual handling required: %s', self.dst)
        copied = copy(self.src, errordir)
        logger.debug('file copied to %s', copied)
        if not args.safe:
            self.remove_original()
            self.done = True
        else:
            logger.debug('\tWould do %s.remove_original()', self)
        logger.warning('\tCheck it out at %s', url)

    def copy_over(self):
        logger.info('\t%s → %s', self.src, self.dst)
        os.makedirs(self._dstdirname, mode=0o775, exist_ok=True)
        with open(self.src, "rb") as src, open(self.dst, "a") as dst:
            dst.writelines([LogLine(line, self).decode() for line in src])

    def remove_original(self):
        assert(not args.safe)
        logger.debug("\tdeleting original log %s...", self.src)
        os.remove(self.src)

    def log2html(self):
        logger.info("\tlogs2html...")
        call(["logs2html", self._dstdirname])

    def gen_pisg_config(self):
        logger.info("\tgenerating pisg configuration...")
        Pisg.create_config(self)


class Loghandler:
    def __init__(self):
        Lock.lock()
        if not args.repick:
            self.find_and_move()
        else:
            logger.debug('Script called with the "repick" option. Not '
                         'running find_and_move()')
        self.load_logs()
        self._find_dupped_chans()
        total = len(self.logs)
        i = 0
        while i < total:
            logger.info('Iterating... %d/%d:', i+1, total)
            self.handle(self.logs[i])
            i = i + 1
        if args.repick or args.nopisg:
            logger.info('Not running pisg: called with "repick" or "nopisg"')
        else:
            Pisg.run()
        if not args.safe:
            for log in self.logs:
                assert(log.done)
        Lock.unlock()

    def load_logs(self):
        self.logs = [Log(x) for x in os.listdir(conf.originlog)]
        if len(self.logs) == 0:
            logger.info("No logs to parse available")

    @staticmethod
    def find_and_move():
        znc_logdir = '/srv/znc/moddata/log/'
        logger.info("Starting find_and_move from [%s]...", znc_logdir)
        for f in os.scandir(znc_logdir):
            assert f.is_file(follow_symlinks=False)
            dst = os.path.join(conf.originlog, f.name)
            if os.access(dst, os.F_OK):
                logger.error("The target path already exists: %s", dst)
                logger.error("Skipping find_and_move() for %s", f.name)
            logger.debug("Moving %s to %s", f.path, dst)
            if not args.safe:
                move(f.path, dst)
            else:
                logger.info("\twould do: move(%s, %s)", f.name, dst)

    def _find_dupped_chans(self):
        logger.debug('looking for duplicates channels...')
        logs = [(x.network, x.channel, x.date)
                for x in self.logs if not x.is_private()]
        self.duplicates = set([x for x in logs if logs.count(x) > 1])

    def handle(self, log):
        logger.info("Processing: %s", log)
        if log.done:
            logger.info('\tThis log is already marked as done, skipping')
            return
        if (log.network, log.channel, log.date) in self.duplicates and \
                not log.dup_override:
            return self._handle_dup(log)
        if log.is_private():
            logger.debug('\tThis log is private')
        if not args.safe:
            if log.is_query() and log.user != 'mapreri':
                logger.info('\tThis log is query.')
                logger.info("Only query mapreri's are saved, removing this.")
                log.remove_original()
            else:
                log.copy_over()
                log.remove_original()
                if not log.is_private():
                    log.log2html()
                    log.gen_pisg_config()
            log.done = True
        else:
            logger.info('\twould do: %s → %s', log.src, log.dst)
            logger.info('\twould do: .remove_original()')

    def _handle_dup(self, log):
        """
        This method checks the duplicated logs to find out if they are
        effectively duplicated, parsing one line at time (excluding the
        timestamp), and do things (e.g. call handle() to to store the log).
        Currently it's able to handle only duplicated logs, not triplicated or
        more.
        """
        identifier = (log.network, log.channel, log.date)
        logger.info('\tThis log is duplicated, extra-processing it...')
        dups = [x for x in self.logs if
                (x.network, x.channel, x.date) == identifier]
        logger.debug('\tThe duplicates:')
        for i in dups:
            logger.debug('\t\t%s', i)
        if len(dups) > 2:
            logger.warning('\t\tunable to handle more than 2 duplicated '
                           'logs for the same network/channel')
            for logfile in dups:
                logfile.errorlog('DuplicatedLog3Users')
            return
        if len(dups) == 1:
            logger.critical('nonsense: looks like I have one duplicated '
                            'log and this does not compute.  Datails: %s',
                            dups[0])
            crash(ValueError)
        # ok, we have only 2 apparently duplicated logs
        log1 = dups[0]
        log2 = dups[1]
        if filecmp.cmp(log1.src, log2.src, shallow=False):
            # somehow these two logs are exactly identical!
            logger.info('\tThese logs are actually the same, discarding one '
                        'and saving the other')
            log1.dup_override = True
            self.handle(log1)
            if not args.safe:
                log2.remove_original()
                log2.done = True
            else:
                logger.info('\twould do: %s.remove_original()' % log2)
            return
        # load everything in memory, more easy to handle and anyway we
        # won't have to deal with huge logs (I hope)
        with open(log1.src, 'rb') as fd1, open(log2.src, 'rb') as fd2:
            lines1 = fd1.readlines()
            lines2 = fd2.readlines()
        if len(lines1) != len(lines2):
            logger.warning('\tthese two duplicated logs have different '
                           'lenghts, so they are definitely different')
            log1.errorlog('DuplicatedLogChecked')
            log2.errorlog('DuplicatedLogChecked')
            return
        # last chance for them to be the same, strip out timestamp
        # and trailing whitespaces from the lines
        with open(log1.src, 'rb') as fd:
            for line in fd:
                try:
                    dline = LogLine(line, log1).decode(error=True).strip()
                except UnicodeDecodeError:
                    logger.error('\tDecode error in this duplicated log.')
                    log1.errorlog('DecodeDuplicatedLog')
                    log2.errorlog('DecodeDuplicatedLog')
                    return
                lines1.append(re.split(r'^\[\d\d:\d\d:\d\d\] ', dline)[1])
        with open(log2.src, 'rb') as fd:
            for line in fd:
                try:
                    dline = LogLine(line, log2).decode(error=True).strip()
                except UnicodeDecodeError:
                    logger.error('Decode error in this duplicated log.')
                    log1.errorlog('DecodeDuplicatedLog')
                    log2.errorlog('DecodeDuplicatedLog')
                    return
                lines2.append(re.split(r'^\[\d\d:\d\d:\d\d\] ', dline)[1])
        # check now the content (without timestamp and trailing whitespaces)
        i = 0
        tot_lines = len(lines1)
        while i < tot_lines:
            if lines1[i] != lines2[i]:
                logger.warning('\tthese duplicated files actually differs')
                log1.errorlog('DuplicatedLogChecked')
                log2.errorlog('DuplicatedLogChecked')
                return
            i += 1
        logger.info('\t\tso, these two logs are the same, copying one and '
                    'discarding the other')
        log1.dup_override = True
        self.handle(log1)
        if not args.safe:
            log2.remove_original()
            log2.done = True
        else:
            logger.info('\twould do: %s.remove_original()' % log2)


if __name__ == '__main__':

    # catch a couple of command line options
    parser = argparse.ArgumentParser()
    parser.add_argument('-c', '--config', required=False,
                        help='use a different config file')
    parser.add_argument('-d', '--debug', action='store_true',
                        help='enable debug and print to stdout')
    parser.add_argument('-f', '--force', action='store_true',
                        help='ignore presence of lock file.  Also do not '
                        'create or destroy any lock file already existing')
    parser.add_argument('-n', '--nopisg', action='store_true')
    parser.add_argument('-r', '--repick', action='store_true',
                        help='do not take new log files, continue processing '
                        'the current queue')
    parser.add_argument('-s', '--safe', action='store_true',
                        help='do not actually touch files')
    parser.add_argument('-v', '--verbose', action='store_true',
                        help='enable verbose output (INFO level) to stdout')
    args = parser.parse_args()
    if args.config:
        print('Warning: using a non-default config file: %s' % args.config)
        conf = Configuration(args.config)

    logger = logging.getLogger(__name__)
    # set the global level to the lowest value, so each handler can set its own
    logger.setLevel(logging.DEBUG)
    if not args.debug:  # debug messages to file, errors to console
        fh = logging.FileHandler(conf.logfile)
        fh.setLevel(logging.DEBUG)
        ch = logging.StreamHandler()
        ch.setLevel(logging.INFO if args.verbose else logging.ERROR)
    else:  # output all messages both to file and console
        fh = logging.FileHandler(conf.logfile)
        fh.setLevel(logging.DEBUG)
        ch = logging.StreamHandler()
        ch.setLevel(logging.DEBUG)
    # create formatter and add it to the handlers
    formatter = logging.Formatter('%(asctime)s - %(levelname)s:\t%(message)s', "%Y-%m-%dT%H:%M:%SZ")
    ch.setFormatter(formatter)
    fh.setFormatter(formatter)
    # add the handlers to logger
    logger.addHandler(ch)
    logger.addHandler(fh)

    logger.debug('Loaded configuration: %s', conf.__dict__)

    Lock.check_or_quit(timeout=60*29)
    try:
        Loghandler()
    except Exception as e:
        crash(e)
    except KeyboardInterrupt:
        logger.warning('Keyboard Interrup.  Exiting…')
        sys.exit(0)
    logger.info('And also this time all is finished well :)')
