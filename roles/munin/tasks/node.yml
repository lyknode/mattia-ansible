---

- name: Install the munin node package
  apt:
    package: munin-node
    state: present

- name: install munin node config
  template: src=munin-node.conf
            dest=/etc/munin/munin-node.conf
            owner=root
            group=munin
            mode=0644
  notify: restart munin-node

- name: install munin plugins config
  template: src=plugins.conf
            dest=/etc/munin/plugin-conf.d/munin-node
            owner=root
            group=root
            mode=0644
  notify: restart munin-node

- name: let the plugin conf files be readable
  file: path=/etc/munin/plugin-conf.d state=directory mode=0755

- name: enable plugins
  file: path=/etc/munin/plugins/{{ item.key }}{{ item.suffix|d('') }}
        src=/usr/share/munin/plugins/{{ item.key }}
        state=link
        owner=root
        group=root
  notify: restart munin-node
  with_items: "{{ munin_plugins_enabled|default([]) }}"

- name: disable plugins
  file: path=/etc/munin/plugins/{{ item.key }}{{ item.suffix|d('') }}
        state=absent
  notify: restart munin-node
  with_items: "{{ munin_plugins_disabled|default([]) }}"

- name: check whether the manual installed plugins are the regular file
  stat: path=/etc/munin/plugins/{{ item }}
  with_items: "{{ munin_plugins_install|default([]) }}"
  register: manual_plugins

- name: remove them if they are symlink
  file: path=/etc/munin/plugins/{{ item.item }} state=absent
  when:
    - item.stat.exists
    - item.stat.islnk
  with_items: "{{ manual_plugins.results|default([]) }}"

- name: install plugins
  template: src=plugins/{{ item }} dest=/etc/munin/plugins/{{ item }} follow=no force=yes owner=root group=root mode=0755
  notify: restart munin-node
  with_items: "{{ munin_plugins_install|default([]) }}"

- name: install dependencies (using APT)
  apt:
    package: "{{ munin_apt_packages|default([]) }}"
    state: present
  notify: restart munin-node

- name: install an eventual systemd service to start a ssh tunnel
  template: >
    src=munin-ssh-tunnel.service
    dest=/etc/systemd/system/munin-ssh-tunnel.service
    owner=root
    group=root
    mode=0644
  notify: reload systemd-tunnel
  with_items: "{{ ssh_hosts }}"
  when: inventory_hostname in ssh_hosts|map(attribute='host')|list
