#
# {{ ansible_managed }}
#

[DEFAULT]
cleaner = fakeroot debian/rules clean
# use pristine-tar:
pristine-tar = True
# Use color when on a terminal, alternatives: on/true, off/false or auto
color = true
# automatically GPG sign tags:
{% if "workstation" in group_names %}
sign-tags = True
{% else %}
sign-tags = False
{% endif %}
filter = [
       'CVS',
       '.cvsignore',
       '.hg',
       '.hgignore',
       '.bzr',
       '.bzrignore',
       '.git',
       '.gitignore'
       ]
# don't silently put DEBFULLNAME/DEBEMAIL in .git/config (clone, import-dsc)
repo-user = GIT
repo-email = GIT

[buildpackage]
# Look for a tag matching the upstream version when creating a tarball
#upstream-tree = tag
# keyid to GPG sign tags with:
#keyid = 0xdeadbeef
# push to a remote repository after a successful tag:
#posttag = git-push git.example.com
# call lintian after a successful build:
#postbuild = lintian $GBP_CHANGES_FILE
# let package generate upstream changelog before build:
#prebuild = GIT_DIR=$GBP_GIT_DIR debian/autogen.sh
# use this for more svn-buildpackage like behaviour:
#export-dir = ../build-area/
#tarball-dir = ../tarballs/
#ignore-new = True
#export = HEAD
# compress with bzip2
#compression = bzip2
# use best compression
#compression-level = best
# Don't send notifications, alternatives: on/true, off/false or auto
#notify = off
# Transparently handle submodules
# submodules = True
# Wheter to use cowbuilder via git-pbuilder(1)
#pbuilder = True
# Which distribution to use with git-pbuilder
#dist = testing
# Options to pass to pbuilder when using git-pbuilder
#git-pbuilder-options = '--hookdir /etc/pbuilder/hooks'

# Options only affecting gbp import-orig
[import-orig]
#filter-pristine-tar = True
# run hook after the import:
postimport = gbp dch -N%(version)s -a --debian-branch=$GBP_BRANCH
#compression = xz
# commit message:
#import-msg = New upstream version %(version)s

# Options only affecting gbp import-dsc
[import-dsc]
#force committer to be the same as author
#author-is-committer = True
#same for the date
#author-date-is-committer-date = True

# Options only affecting gbp dch
[dch]
# options passed to git-log:
#git-log = --no-merges
# next snapshot number:
#snapshot-number = snapshot + 1
# include 7 digits of the commit id in the changelog enty:
id-length = 0
# don't include information from meta tags:
#meta = False
# what tags to look for to generate bug-closing changelog entries:
meta-closes = Closes|LP
# include the full commit message in the changelog:
full = True
# ignore Signed-off-by: lines:
ignore-regex = (Signed-off|Acked)-by:
# use author name and email from git-config for the trailing line:
git-author = False
# keep only one paragraph per author:
multimaint = True
multimaint-merge = True

# Options only affecting gbp pq
[pq]
#patch-numbers = False

# Options only affecting gbp clone
[clone]
#pristine-tar = True

# Options only affecting gbp pull
[pull]
#pristine-tar = True

# Options only affecting gbp create remote repo
[create-remote-repo]
# disable remote branch tracking
#track = False
