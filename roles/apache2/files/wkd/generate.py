#! /usr/bin/env python3

# Copyright: © 2019 Daniel Kahn Gillmor <dkg@fifthhorseman.net>
#            © 2021 Mattia Rizzolo <mattia@mapreri.org>
# License: GPL-3+

import os
import codecs
import hashlib
import subprocess
from email.utils import parseaddr


def wkd_localpart(incoming: bytes) -> str:
    '''Z-base32 the localpart of an e-mail address

    https://tools.ietf.org/html/draft-koch-openpgp-webkey-service-08#section-3.1
    describes why this is needed.

    See https://tools.ietf.org/html/rfc6189#section-5.1.6 for a
    description of the z-base32 scheme.
    '''
    zb32 = "ybndrfg8ejkmcpqxot1uwisza345h769"

    b = hashlib.sha1(incoming).digest()
    ret = ""
    assert(len(b) * 8 == 160)
    for i in range(0, 160, 5):
        byte = i // 8
        offset = i - byte * 8
        # offset | bits remaining in k+1 | right-shift k+1
        # 3 | 0 | x
        # 4 | 1 | 7
        # 5 | 2 | 6
        # 6 | 3 | 5
        # 7 | 4 | 4
        if offset < 4:
            n = (b[byte] >> (3 - offset))
        else:
            n = (b[byte] << (offset - 3)) + (b[byte + 1] >> (11 - offset))

        ret += zb32[n & 0b11111]
    return ret


def get_localpart(address: str) -> (bytes, str):
    _name, addr = parseaddr(address)
    localpart, _at, thisdomain = addr.rpartition('@')
    return (localpart.lower().encode(), thisdomain)


def emit_wkd(localpart: bytes, domain: str) -> None:
    wkdstr = wkd_localpart(localpart)
    addr = codecs.decode(localpart) + '@' + domain
    cmd = (
        'gpg', '--batch', '--no-options', '--with-colons',
        '--output', os.path.join('openpgpkey', domain, 'hu', wkdstr),
        '--export-options', 'export-clean',
        '--export-filter', 'keep-uid=mbox=' + addr,
        '--export', addr
    )
    subprocess.run(cmd)


def build_wkd() -> None:
    domain = "mapreri.org"
    os.makedirs(os.path.join('openpgpkey', domain, 'hu'), exist_ok=True)

    addr_list = [
        "mattia@mapreri.org",
    ]

    for uid in addr_list:
        emit_wkd(*get_localpart(uid))


if __name__ == "__main__":
    build_wkd()
